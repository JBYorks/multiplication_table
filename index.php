<?php
declare(strict_types = 1);

include_once('timesTable.php');      
?>
<!DOCTYPE html>
<html lang="en-GB">
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" 
    href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
    integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" 
    crossorigin="anonymous">
    <link rel="stylesheet" href="styles/tableStyle.css">
    <title>Times Table</title>
  </head>
    <body>
      <div class="container-fluid headerBox text-center">
        <h1 id="header">Multiplication Table</h1>
      </div>
        
      <div class="container-fluid formBox text-center">
          <form action="index.php" method="POST">
          <label for="rows">Rows:</label><input type="number" id="rows" 
          name="rows" placeholder="Enter value:" autofocus required>
          <label for="columns">Columns:</label><input type="number" 
          id="columns" name="columns" placeholder="Enter value:" required>
          <input  type="submit" value="Create">
        </form>
      </div>
        
      <div style="text-align:center;">
        <?php
            if (isset($_POST['rows'], $_POST['columns'])) {
                $rows = ($_POST['rows']);
                $cols = ($_POST['columns']);
                if ($rows <= 0 || $cols <= 0) {
                    echo "Please only set values greater than zero";
                    $mulTable = new Multiplication(10, 10);
                } else {
                    echo "$rows x $cols times table";
                    $mulTable = new Multiplication((int) $rows, (int) $cols);
                }
            } else {
                echo "Please enter values to create custom multiplication grid";
                $mulTable = new Multiplication(10, 10);  
            }
            echo $mulTable->web(); 
        ?>     
      </div>
    </body>
</html>
