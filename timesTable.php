<?php
declare(strict_types=1);

class Multiplication
{
    private $rows;
    private $cols;

    public function __construct(int $rows, int $cols)
    {
        $this->rows = $rows;
        $this->cols = $cols;
    }
    /**
    * This function creates a html table on the web application and highlights
    * and sets the styling of any data in the very first row and column to the 
    * designated css, whilst multiplying the other data.
    * @return string $table
    */
    public function web() :string
    {
        $table = "<table class=\"center\">";
        for ($row = 0; $row <= $this->rows; $row++) {
            $table .= "<tr>";
            for ($column = 0; $column <= $this->cols; $column++) {
                if ($row == 0 && $column == 0) {
                    $table .= "<th class=\"th\">x</th>";
                } elseif ($row == 0) {
                    $table .= "<th class=\"th\">" . $column . "</th>";
                } elseif ($column == 0) {
                    $table .= "<th class=\"th\">" . $row . "</th>";
                } else {
                    $table .= "<td>" . ($row * $column) . "</td>";
                }
            }
            $table .= "</tr>";
        }
        $table .= "</table>";
        return $table;
    }
    /**
    * This function creates a cli table by multiplying input parameters and
    * setting the spacing, based on the input length.
    * @return string $table
    */
    public function cli() :string
    {
        $table = "";  
            for ($row = 0; $row <= $this->rows; $row++) {
                for ($column = 0; $column <= $this->cols; $column++) {
                    if ($row == 0 && $column == 0) {
                        $output = "\033[0;31;41m" . str_pad("x", 6, " ", STR_PAD_BOTH) . "\033[0m" . "|";
                    } elseif ($row === 0) {
                        $output = "\033[0;31;47m" . str_pad("$column", 6, " ", STR_PAD_BOTH) . "\033[0m" . "|";
                    } elseif ($column === 0) {
                        $output = "\033[0;31;47m" . str_pad("$row", 6, " ", STR_PAD_BOTH) . "\033[0m" . "|";
                    } else {
                        $len = $row * $column;
                        $output  = str_pad("$len", 6, " ", STR_PAD_BOTH) . "|";
                    }
                    $table .= $output;
                }
                $table .= "\n";
            }
            return $table;
    }
}
