<?php
declare(strict_types=1);

include_once ('timesTable.php');

echo "  |-- CUSTOM MULTIPLICATION TABLE --|\n";

if($argc < 2) {
    $mulTable = new Multiplication(10, 10);
} else {
    $rows = ((int)$argv[1]);
    $cols = ((int)$argv[2]);
    $mulTable = new Multiplication($rows, $cols);  
} 
echo $mulTable->cli();
