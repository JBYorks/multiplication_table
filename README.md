# Instructions to use the PHP Multiplication Tables in HTML and Command Line Interface (CLI)

The files were created using:  
* - NetBeans IDE 8.2  
* - Git Bash for Windows (MINGW64)  


---

## HTML

1. Load the files **index.php** and **timesTable.php** to your IDE.
2. Run the index page which should load up with the deafult multiplication table and the input form.
3. Input the desired values and click on *'Create'*.
4. Any value that is a *zero* or less will revert back to the default table, with a message to alert the user to input higher values.

![web app screenshot](images/imageHTML.JPG)

---

## CLI

1. Locate the files **tableCLI.php** and **timesTable.php** using CLI navigation.
2. Run the default 10 x 10 multiplication table by executing *php tableCLI.php*
3. Set the custom multiplication table by using the command: 
	* *php tableCLI.php ```5 7```*.
	* The first number is the __row__ the second is the __column__.
	* So in this instance the table would have __5 rows__ and __7 columns__. 
4. Change to desired values.

(Please note your version should have the first row and column highlighted. Unfortunately my version of Git Bash on Windows 7 doesn't convert the colours, and so I have not included it on the screenshot)

![CLI screenshot](images/imageCLI.JPG)


---
